.. SecKit SA IDM Common documentation master file, created by

.. _using:

================================================
Using Common Assets
================================================

Before continuing with this section ensure you have completed the quickstart tutorial.

Enrichment Lookups
-------------------------------------------

.. _location:

seckit_idm_pre_cidr_location
++++++++++++++++++++++++++++++++++++++++++++

Geolocation data is helpful context to security investigation. Location and give insight into the
contextual appropriateness of actions within a network such as does the person belong in the facility involved in the event. Some discretion is advised when configuring location data to avoid creation of low value administrative burden. The lowest resolution useful should be consistently used for example the lat/long of the main entrance for a large campus rather than attempting to record building level accuracy.


*Don't* include VPN address ranges used by client VPN technology in the location table as the same location as the data center. Identify a fixed location for all VPN traffic not also used by a real facility.

*Optional* in the TOOLS folder leverage ``Security Kit Location and Categories Tables.xlsx`` to develop the location list.

cidr
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

A CIDR block allocated to a specific location the largest non overlapping block should be used

lat & long
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Standard notation not more than 5 digits of precision

city
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The city name

state
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Postal abbreviation for state or province typically two char english uppercase

Country
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Country abbreviation typically two char english upper case

seckit_idm_pre_cidr_category
++++++++++++++++++++++++++++++++++++++++++++

cidr_priority
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Minimum priority applied for notable correlation in Splunk Enterprise Security

- [blank] No value indicates no specific priority is applied to this cidr
- low
- medium
- high
- critical

cidr_pci_domain
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This field is often overloaded to indicate additional specific regulatory relationships example uses

- pci = In scope for PCI assessment included wifi networks and control networks for card holder data
- cardholder = Systems contain card holder data
- GDPR = similar to PCI contains control systems but not actual data
- PII = contains personally identifiable information


cidr_bunit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Business unit or department. Splunk Enterprise security will combine all values of bunit as a multi value field. At your discretion "top" and lower level values can be applied OR should be applied only to the lowest level underwhich ther should be no smaller levels for example

10.1.0.0/20 is allocated to the "hospital" bunit however 10.1.14.0/24 is allocated to surgery. If the bunit is provided for both CIDR blocks at search time the field bunit will contain both values.

cidr_owner
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The owner (typically email) of a user, group or point of contact for an asset. In most organizations this is only provided on small /22 or /24 subnets which contain systems under the responsiblity of a single group. In most cases this field is blank

cidr_category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following categories are commonly defined in the categories configuration per cidr. The shortest reasonable string should be used for all values. Note only values matching the regex [A-Za-z0-9\-_] should be used. See the categories chapter for specific examples.

Apply the updated configuration to your assets
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Update the configuration files using Enterprise Security Content Management*

- As a es_admin login to Splunk Enterprise Security
- Navigate to the configure menu
- Select Content Management
- Select "SecKit SA IDM Common" from the app menu
- Find "SecKit IDM Common network location" by name and click update file upload the file created above ``seckit_idm_pre_cidr_location.csv``
- Find "SecKit IDM Common network categories" by name and click update file upload the file created above ``seckit_idm_pre_cidr_category.csv``

*Force Merge of Assets*

The following process can be used at any time to force immediate updates of asset files

- Navigate to a Splunk Search window
- Run the search ``| savedsearch "seckit_idm_common_assets_networks_lookup_gen"``
- Run the search ``| from savedsearch:"Identity - Asset String Matches - Lookup Gen"``
- Run the search ``| from savedsearch:"Identity - Identity Matches - Lookup Gen"``

*Verification*

- As an ES user (or above) navigate to Enterprise security
- Select Security Domains from the menu
- Select Identity from the drop down
- Select Asset Center
- View the record as defined above if additional records are displayed from other sources sort/scroll to locate

Scheduled Searches and Enabled Input Tasks
-------------------------------------------

Inputs
++++++++++++++++++++++++++++++++++++++++++++++++++++

identity_manager://seckit_idm_common_assets_networks
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Utilized to enable the usage of the main combined lookup by Enterprise Security Identity Manager

Scheduled Searches
++++++++++++++++++++++++++++++++++++++++++++++++++++

seckit_idm_common_assets_networks_lookup_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Produces the lookup ``seckit_idm_common_assets_networks_lookup`` used as input in ``identity_manager://seckit_idm_common_assets_networks``. The default schedule will produce a new lookup every 4 hours.


seckit_idm_combined_cidr_category_by_str_lookup_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Combines the csv lookup ``seckit_idm_pre_cidr_category_by_str_lookup`` and search managed collection ``seckit_idm_common_event_cidr_category`` to produced the lookup ``seckit_idm_combined_cidr_category_by_str_lookup``. This lookup is utilized by the saved search ``seckit_idm_common_assets_networks_lookup_gen`` to produce the network assets file. The default schedule will produce a new file every 30 min.

seckit_idm_common_event_cidr_category_from_dm_network_session_dhcp
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Utilizes the network session data model to identify network segments managed using DHCP to automatically categorize subnets. The default schedule will detect new subnets every 4 hours.


seckit_idm_common_event_cidr_category_age
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ages entries in the lookup ``seckit_idm_common_event_cidr_category_age`` where last is non zero and not updated in the prior year. The default schedule will trim the lookup once per day


seckit_idm_common_assets_expected_tracker_gen
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Updates the lookup ``seckit_idm_common_assets_host_expected_tracker_lookup`` based on universal forwarder internal logs to identify hosts which should be set as is_expected. The default schedule search runs at the top of the hour using only the last 15m of prior data.

seckit_idm_common_assets_expected_tracker_age
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ages entries in the lookup ``seckit_idm_common_assets_host_expected_tracker_lookup`` not updated in the prior year. The default schedule will trim the lookup once per day.

seckit_idm_pre_cidr_category_by_str_lookup_ftr
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ensures the lookup ``seckit_idm_pre_cidr_category_by_str_lookup`` exists and contains the correct fields. The default schedule of the search uses a special configuration option run_on_startup and run_n_times to ensure the search runs on only once.

seckit_idm_common_assets_networks_lookup_ftr
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ensures the lookup ``seckit_idm_common_assets_networks_lookup`` exists and contains the correct fields. The default schedule of the search uses a special configuration option run_on_startup and run_n_times to ensure the search runs on only once.

seckit_idm_pre_host_static_lookup_ftr
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ensures the lookup ``seckit_idm_pre_host_static_lookup`` exists and contains the correct fields. The default schedule of the search uses a special configuration option run_on_startup and run_n_times to ensure the search runs on only once.
