.. SecKit SA IDM Common documentation master file, created by

.. _categories:

================================================
Common Categories
================================================

The following categories are commonly defined in the categories configuration. The shortest reasonable string should be used for all values. Note only values matching the regex [A-Za-z0-9\-_] should be used. This reference is shared by the SecKit IDM family of tools each example is marked if it is appropriate for CIDR, a single ASSET, or BOTH

facility_id:<value> CIDR
+++++++++++++++++++++++++++++++++++

The facility_id is typical a short identification string defined by the organizations facility management department. This would be associated to a real physical location under the control of the organization.

facility_type:<value> CIDR
+++++++++++++++++++++++++++++++++++

The facility type reflect the general purpose of the facility common examples

- DC = Data Center
- COLO = Colocation
- STORE = Retail Store
- OFFICE = General Office
- PLANT = Plant

known_scanner BOTH
++++++++++++++++++++++++++++++
A known scanner will regularly trigger vuln scanner detection this category is applied to all types of scanners.

known_scanner:<value> BOTH
++++++++++++++++++++++++++++++

In addition to known_scanner this category with a value gives context to the expected type of scanner.

- vuln = Network vulnerability scanner such as rapid7 or nessus
- app = Application scanner such as burp suite used to detect application vulnerabilities
- ping = Ping scanner typically used to monitor uptime for IT operations
- alive = Application is alive scanner typically can understand application level response
- asset = Asset discovery or collection system

net_assignment:<value> CIDR
+++++++++++++++++++++++++++++++++++

The network address management method for the segment

- static = IP address is assigned locally
- dyndhcp = IP address is issued by a DHCP server
- dynvirt = IP address is issued by a virtualization system such as docker, AWS, hyper-v or vcenter and rarely changes

net_type:<value> CIDR
+++++++++++++++++++++++++++++++++++

The network type

- RFC1918 (Reserved per RFC)
- CGNAT (Carrier Grade NAT)


pf:<value> BOTH
+++++++++++++++++++++++++++++++++++

The PF or primary function of a device is a specific identifier relates to the role of a asset in a service. This is most commonly applied to a specific asset but may apply to a CIDR

svc:<value> BOTH
+++++++++++++++++++++++++++++++++++

The SVC or Service is a identifier indicating the service this asset participates in providing for example. The service DNS "svc:DNS" would typlically be made up of a combination of "pf:ms_dns" or "pf:BIND" AND "pf:dns_rbl" "pf:dns_recursive"

zone:<value> CIDR
+++++++++++++++++++++++++++++++++++

The network zone type for the network segment common values are as follows

- LAN = Wired lan
- DMZ
- WLAN = Wireless Lan
- WWLAN = Mobile or wireless private networking (Rare)
- VPN = Client VPN
- PartnerVPN = Site 2 Site  VPN network with a partner
- Storage = Storage area network
- Guest = Guest network wired or wired
- Vendor = Vendor equipment network (devices not managed by org)
- IC = Industrial Controls
- SAFETY = Life or Safety systems

zone_name:<value> CIDR
+++++++++++++++++++++++++++++++++++

Many organizations have more than one of a specific type of zone particularly DMZ, and VPN zones the name this field can be used to specifically identify those zones.


Apply the updated configuration to your assets
----------------------------------------------

*Update the configuration files using Enterprise Security Content Management*

- As a es_admin login to Splunk Enterprise Security
- Navigate to the configure menu
- Select Content Management
- Select "SecKit SA IDM Common" from the app menu
- Find "SecKit IDM Common network location" by name and click update file upload the file created above ``seckit_idm_pre_cidr_location.csv``
- Find "SecKit IDM Common network categories" by name and click update file upload the file created above ``seckit_idm_pre_cidr_category.csv``

*Force Merge of Assets*

The following process can be used at any time to force immediate updates of asset files

- Navigate to a Splunk Search window
- Run the search ``| savedsearch "seckit_idm_common_assets_networks_lookup_gen"``
- Run the search ``| from savedsearch:"Identity - Asset String Matches - Lookup Gen"``
- Run the search ``| from savedsearch:"Identity - Identity Matches - Lookup Gen"``

*Verification*

- As an ES user (or above) navigate to Enterprise security
- Select Security Domains from the menu
- Select Identity from the drop down
- Select Asset Center
- View the record as defined above if additional records are displayed from other sources sort/scroll to locate
