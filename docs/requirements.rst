.. SecKit SA IDM Common documentation master file, created by

.. _requirements:

================================================
Splunk System Requirements
================================================

Mandatory
-------------------------------------------
- Splunk Enterprise >7.1.0
- Splunk Enterprise Security >5.1.0

Review size of lookups in memory
-------------------------------------------

Splunk utilizes a default maximum size of in memory lookup tables that can be exceeded when large numbers of CIDR assets are tracked in enterprise security. If the size is exceeded a user error will appear to the effect of CIDR match lookup can not used indexed lookups. Should the impact the environment review the size of the enterprise security lookup assets_by_cdr.csv and increase the value limits.conf/[lookup]/max_memtable_bytes to 125% of the size of this file in bytes and apply the change to both the ES Search Head and the indexer tier.

Optional - Splunk Stream
-------------------------------------------

To use Splunk Stream to discovery DHCP subnets the following patch must be applied


.. code-block:: ini
  :caption: $SPLUNK_HOME/apps/Splunk_TA_stream/local/props.conf
  :name: props

  [stream:dhcp]
  FIELDALIAS-lease_scope = subnetmask AS lease_scope
