.. SecKit SA IDM Common documentation master file, created by

.. _quickstart:

================================================
Quick Start Tutorial
================================================

The quick start procedure is simply to demonstrate the application of this solution continue
reading in the using guide once your first use is complete.

Identify subnets for use
-------------------------------------------

- Working with a knowledgeable network administrator identify one network larger
  than a ``24/`` that is located in a data center.
- For this specific data center identify
  - City, State/province postal code, country code (2 US letter code)
  - Street Address
  - Using Street address use a service such as google maps to identify lat, lon to 4 digits of precision


Configure SecKit to label the network asset
-------------------------------------------

*Create the initial configuration files*

Create (or copy and paste these examples) on your computer and then we will upload them to ES.

- Create a csv file on your computer as follows named ``seckit_idm_pre_cidr_location.csv``

+--------------+----------+-----------+---------------+-----------+-----------+
| cidr         | lat      | long      | city          | state     | country   |
+--------------+----------+-----------+---------------+-----------+-----------+
| 10.0.0.0/16  | 37.7826  | -122.3934 | San Francisco | CA        | US        |
+--------------+----------+-----------+---------------+-----------+-----------+

- Create a csv file on your computer as followed name ``seckit_idm_pre_cidr_category.csv`` empty cells are left blank for now

+--------------+----------------------+----------------------------------------+---------------+------------+------------+
| cidr         | cidr_pci_domain      | cidr_category                          | cidr_priority | cidr_bunit | cidr_owner |
+--------------+----------------------+----------------------------------------+---------------+------------+------------+
| 10.0.0.0/16  |                      | facility_type:dc|zone:lan              |               |            |            |
+--------------+----------------------+----------------------------------------+---------------+------------+------------+

*Update the configuration files using Enterprise Security Content Management*

- As a es_admin login to Splunk Enterprise Security
- Navigate to the configure menu
- Select Content Management
- Select "SecKit SA IDM Common" from the app menu
- Find "SecKit IDM Common network location" by name and click update file upload the file created above ``seckit_idm_pre_cidr_location.csv``
- Find "SecKit IDM Common network categories" by name and click update file upload the file created above ``seckit_idm_pre_cidr_category.csv``

*Force Merge of Assets*

The following process can be used at any time to force immediate updates of asset files

- Navigate to a Splunk Search window
- Run the search ``| from savedsearch: "seckit_idm_common_assets_networks_lookup_gen"``
- Run the search ``| from savedsearch: "Identity - Asset String Matches - Lookup Gen"``
- Run the search ``| from savedsearch: "Identity - Asset CIDR Matches - Lookup Gen"``

*Verification*

- As an ES user (or above) navigate to Enterprise security
- Select Security Domains from the menu
- Select Identity from the drop down
- Select Asset Center
- View the record as defined above if additional records are displayed from other sources sort/scroll to locate
