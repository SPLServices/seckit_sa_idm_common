.. SecKit SA IDM Common documentation master file, created by

.. _install:

================================================
Installation
================================================

Installation of the apps is intended to be minimally impactful to a Splunk ES environment.
If existing assets and identities have been configured care should be take to ensure an asset
or identity is only defined once.

Migration from legacy assets and identities
-------------------------------------------

Enterprise Security does not "merge" records from multiple sources having multiple
conflicting definitions can impact systems active users.

- Identify and remove the identity file definition from Splunk Enterprise Security
- Identify and remove the lookup definition from Splunk Enterprise
- Identify and remove the lookup file from disk. *Important to ensure large bundles do not impact search replication*

Installation
-------------------------------------------

This add on is installed on the Splunk Enterprise Security Search head.

Splunk Enterprise: Choose one, note Splunk Base releases are considered stable

- Download the latest published release from `SplunkBase <https://splunkbase.splunk.com/app/3055/>`_
- Download the latest master build from `bitbucket <https://bitbucket.org/SPLServices/seckit_sa_idm_common/downloads/>`_
- See `installing apps <http://docs.splunk.com/Documentation/AddOns/released/Overview/Wheretoinstall>`_ This add on only requires installation on the search head in a distributed deployment.

Splunk Cloud:

- Using a service request ask for the app installation SecKit_SA_idm_common id "3055" specify version 3.0 or latter
- *WARNING* SecKit_SA_idm_common 3.x is not backwards compatible with SecKit_SA_idm_windows 2.x ensure both apps are updated in the same change if deployed together.

Configure ES App Imports
-------------------------------------------

ES must be configured to see (import) the new application this process needs to be completed only one time

*Configuration*

- As an es_admin navigate to Splunk Enterprise Security
- From the Configure menu select General
- From the General menu select App Imports Update
- Click on "update_es"
- Append ``|(SecKit_[ST]A_.*)`` to the Application Regular Expression`
- Click Save

*Verification*

- As an es_admin navigate to Splunk Enterprise Security
- Click the Search menu
- Click Search again
- Execute the search ``|  inputlookup seckit_idm_network_masks_lookup`` verify results containing netmask column are returned.

Initialize Lookups and Collections
-------------------------------------------

Run the following searches in order

- Navigate to a Splunk Search window
- Run the search ``| from savedsearch: "seckit_idm_common_assets_networks_lookup_gen"``
- Run the search ``| from savedsearch: "Identity - Asset String Matches - Lookup Gen"``
- Run the search ``| from savedsearch: "Identity - Asset CIDR Matches - Lookup Gen"``
