.. SecKit SA IDM Common documentation master file, created by


================================================
Welcome to SecKit SA IDM Common's documentation!
================================================

Success Enablement Content "SecKit" apps for Splunk are designed
to accelerate the tedious or difficult tasks. This application IDM Common
is an add on for Splunk Enterprise Security designed to identify basic network
and enrich the with information that is useful to security incident detection
and response as well as compliance tracking. Following through the quick start
you will be able to answer important questions for a single subnet.

- Where is the asset based on src and or dest ip?
- What is the zone of the network?
- What type of facility is the asset located in?

Before you get started
======================

- Complete Splunk Enterprise Security Administration training
- Review the current Assets and Identities section of the `Administration Manual <http://docs.splunk.com/Documentation/ES/latest/Admin/Addassetandidentitydata>`_
- Review the use of lookup data in Splunk
   - `Lookup Command <https://docs.splunk.com/Documentation/Splunk/latest/Knowledge/LookupexampleinSplunkWeb>`_
   - `CIDR and Matching Rules <http://docs.splunk.com/Documentation/Splunk/7.1.3/Knowledge/Addfieldmatchingrulestoyourlookupconfiguration>`_
- CIDR notation splunk required all notations to be correct i.e. 10.0.0.0/16 NOT 10.0.0.1/16 and less than 32 bits.

Support
======================

- Reporting issues or requesting enhancements `Issue tracking <https://bitbucket.org/SPLServices/seckit_sa_idm_common/issues?status=new&status=open>`_

Known Issues
======================

- Splunk Enterprise has partial support for IPv6 CIDR notation some searches may report errors such as ``Invalid [fe80::/10]: 'fe80::/10' is not a valid IP address or CIDR block`` There is no work around use of IPV6 in CIDR notation can be removed from the lookup files or the errors can be ignored.

.. toctree::
   :maxdepth: 2
   :glob:

   requirements
   install/*
   quickstart
   using
   customizing
   categories
