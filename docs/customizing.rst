.. SecKit SA IDM Common documentation master file, created by

.. _customizing:

================================================
Customizing the add on
================================================

The add on support customization using macros

seckit_idm_common_event_cidr_category_from_dm_network_session_dhcp_custom
--------------------------------------------------------------------------

This macro is utilized in the saved search ``seckit_idm_common_event_cidr_category_from_dm_network_session_dhcp`` and can be used to add additional logic via spl to define categories to the CIDR allocations detected via dhcp.



Building you own lists
--------------------------------------------------------------------------

This add on provides a couple of macros to make the job of building out identity and assets lists a bit easier.

- ``seckit_idm_common_output_identities(lookup_name,key_field,tag)`` used to output a list suitable for identity merge. Accepts events utilizing the standard field names.
  - lookup_name - the defined lookup destination for the data must exists
  -  key_field - not used set to nick,
  -  tag use to attach a a category to trace the source of an identity
  - identity - multivalve field (makemv identity) list valid account names for the identity
  - nick - the unique key for the record should not be repeated in any other source
  - category - a multivalve field (makemv category) list of categories to apply
  - priority - may be a single or multivalve field the highest priority found will apply
- ``seckit_idm_common_output_assets(lookup_name,key_field,tag)`` used to output a list suitable for asset merge. Accepts events utilizing the standard field names.
  - lookup_name - the defined lookup destination for the data must exists
  -  key_field - not used set to nick,
  -  tag use to attach a a category to trace the source of an asset
  -  field notes
  -  category - a multivalve field (makemv category) list of categories to apply
  -  priority - may be a single or multivalve field the highest priority found will apply
  -  should_timesync,should_update,requires_av single or multi value fields. Any value of true will result in true for the final output
- ``seckit_idm_common_get_asset_geo`` used by add on packages to enrich ip address with internal geo coding.
