#
# Seckit Identity Management Common for ES  - A utility add on to properly format Assets and
# Identifies for ES
#
# Copyright 2017-2018 Splunk Inc, <rfaircloth@splunk.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
#Fix for DNS based lookups
[seckit_asset_lookup_by_dns_lookup]
filename = assets_by_str.csv
case_sensitive_match = false
max_matches = 1

[seckit_idm_pre_host_static_lookup]
filename = seckit_idm_pre_host_static.csv
fields_list = static_name,static_category,static_pci_domain,static_priority,static_expected

[seckit_idm_pre_cidr_location_default_lookup]
filename = seckit_idm_pre_cidr_location_default.csv
fields_list = cidr,lat,long,city,state,country

[seckit_idm_pre_cidr_location_by_str_lookup]
filename = seckit_idm_pre_cidr_location.csv
fields_list = cidr,lat,long,city,state,country

[seckit_idm_pre_cidr_location_by_cidr_lookup]
filename = seckit_idm_pre_cidr_location.csv
fields_list = cidr,lat,long,city,state,country
match_type = cidr(cidr)
max_matches = 1

[seckit_idm_pre_cidr_category_default_lookup]
filename = seckit_idm_pre_cidr_category_default.csv
fields_list = cidr,cidr_pci_domain,cidr_category,cidr_priority,cidr_bunit,cidr_owner

[seckit_idm_pre_cidr_category_by_str_lookup]
filename = seckit_idm_pre_cidr_category.csv
fields_list = cidr,cidr_pci_domain,cidr_category,cidr_priority,cidr_bunit,cidr_owner

[seckit_idm_combined_cidr_category_by_str_lookup]
filename = seckit_idm_combined_cidr_category.csv
fields_list = cidr,cidr_pci_domain,cidr_category,cidr_priority,cidr_bunit,cidr_owner

[seckit_idm_combined_cidr_category_by_cidr_lookup]
filename = seckit_idm_combined_cidr_category.csv
fields_list = cidr,cidr_pci_domain,cidr_category,cidr_priority,cidr_bunit,cidr_owner
match_type = cidr(cidr)

[seckit_idm_combined_cidr_category_by_cidr_slookup]
filename = seckit_idm_combined_cidr_category.csv
fields_list = cidr,cidr_pci_domain,cidr_category,cidr_priority,cidr_bunit,cidr_owner
match_type = cidr(cidr)
max_matches = 1

[seckit_idm_common_assets_networks_lookup]
filename = seckit_idm_common_assets_networks.csv
fields_list = ip,mac,nt_host,dns,owner,priority,lat,long,city,country,bunit,category,pci_domain,is_expected,should_timesync,should_update,requires_av
match_type = cidr(ip)
case_sensitive_match = false
replicate = false
#
[seckit_idm_common_assets_networks_slookup]
filename = seckit_idm_common_assets_networks.csv
fields_list = ip,mac,nt_host,dns,owner,priority,lat,long,city,country,bunit,category,pci_domain,is_expected,should_timesync,should_update,requires_av
match_type = cidr(ip)
case_sensitive_match = false
max_matches = 1
replicate = false

[seckit_idm_network_masks_lookup]
filename = seckit_idm_network_masks.csv
fields_list = bits,addresses,hosts,netmask,porton_of_c

[seckit_idm_common_assets_host_expected_tracker_lookup]
collection = seckit_idm_common_asset_expected
external_type = kvstore
fields_list = host,last,reason
case_sensitive_match = false
replicate = true

[seckit_idm_common_assets_host_expected_tracker_slookup]
collection = seckit_idm_common_asset_expected
external_type = kvstore
fields_list = host,last,reason
case_sensitive_match = false
replicate = true
max_matches = 1

[seckit_idm_common_assets_host_expected_tracker_tlookup]
collection = seckit_idm_common_asset_expected
external_type = kvstore
fields_list = host,last,reason
case_sensitive_match = false
replicate = true
time_field = last

[seckit_idm_common_event_cidr_category_lookup]
collection = seckit_idm_common_event_cidr_category
external_type = kvstore
fields_list = cidr,last,category,priority,pci_domain,is_expected,vendor_product,searchname
case_sensitive_match = false
