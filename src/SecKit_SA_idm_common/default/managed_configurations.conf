#
# Seckit Identity Management Common for ES  - A utility add on to properly format Assets and
# Identifies for ES
#
# Copyright 2017-2018 Splunk Inc, <rfaircloth@splunk.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
[lookup:seckit_idm_pre_host_static_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_pre_host_static_lookup
label       = SecKit IDM Common static hosts
description = Used to ensure specific categories and priorities are used for specific hosts with all SecKit IDM add ons
#lookup_type = search
lookup_type = adhoc
editable = 1
#savedsearch = seckit_idm_pre_host_static_lookup_ftr

[lookup:seckit_idm_pre_cidr_location_by_str_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_pre_cidr_location_by_str_lookup
label       = SecKit IDM Common network location
description = Used to label ip data with location information for use in ES assets
lookup_type = adhoc

[lookup:seckit_idm_pre_cidr_category_by_str_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_pre_cidr_category_by_str_lookup
label       = SecKit IDM Common network categories
description = Used to label ip data with category and priority information for use with ES assets
lookup_type = adhoc

[lookup:seckit_idm_combined_cidr_category_by_str_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_combined_cidr_category_by_str_lookup
label       = SecKit IDM Common network categories combined
description = Merged user supplied and dynamic cidr categories
lookup_type = search
editable = 0
savedsearch = seckit_idm_combined_cidr_category_by_str_lookup_gen
expose = 0

[lookup:seckit_idm_combined_cidr_category_by_cidr_lookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_combined_cidr_category_by_cidr_lookup
label       = SecKit IDM Common network categories combined by cidr
description = Merged user supplied and dynamic cidr categories by cidr
lookup_type = adhoc
editable = 0
expose = 0

[lookup:seckit_idm_combined_cidr_category_by_cidr_slookup]
endpoint    = /services/data/transforms/lookups/seckit_idm_combined_cidr_category_by_cidr_slookup
label       = SecKit IDM Common network categories combined first match
description = Merged user supplied and dynamic cidr categories first match
lookup_type = adhoc
editable = 0
expose = 0

[lookup:seckit_idm_common_assets_expected_tracker_lookup]
description = Maintain a lookup of hosts to drive is_expected in es assets by sourcetype
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_common_assets_expected_tracker_lookup
label = SecKit IDM Common assets expected tracker by internal events
lookup_type = search
savedsearch = seckit_idm_common_assets_expected_tracker_gen
expose = 0

[lookup:seckit_idm_common_assets_networks_lookup]
description = Maintain a lookup of hosts to drive is_expected in es assets
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_common_assets_networks_lookup
label = SecKit IDM Common assets for ES
lookup_type = search
savedsearch = seckit_idm_common_assets_networks_lookup_gen
expose = 0

[lookup:seckit_idm_common_event_cidr_category_lookup]
description = Maintain a lookup of cidr and additional category data using search
editable = 0
endpoint = /services/data/transforms/lookups/seckit_idm_common_event_cidr_category_lookup
label = SecKit IDM Common dynamic cidr categories ageout
lookup_type = search
savedsearch = seckit_idm_common_event_cidr_category_age
expose = 0
